﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [RoutePrefix("api")]
    [Route("user")]
    public class UserController : ApiController
    {
        private static List<UserModel> userList = new List<UserModel>();

        [AcceptVerbs("POST")]

        public string Create(UserModel user)
        {
            userList.Add(user);

            return "User registered";

        }

        [AcceptVerbs("PUT")]

        public string Update(UserModel user)
        {
            userList.Where(n => n.Id == user.Id)
                .Select(s =>
                {
                    s.Id = user.Id;
                    s.Name = user.Name;
                    s.Login = user.Login;
                    return s;
                }).ToList();

            return "User updated";
        }

        [AcceptVerbs("DELETE")]
        [Route("user/{id}")]
        public string Delete(int id)
        {
            UserModel user = userList.Where(n => n.Id == id)
                .Select(n => n)
                .First();
            userList.Remove(user);

            return "User Deleted";
        }

        [AcceptVerbs("GET")]
        [Route("user/{id}")]
        public UserModel FindByID(int id)
        {
            UserModel user = userList.Where(n => n.Id == id)
                .Select(n => n)
                .FirstOrDefault();

            return user;
        }

        [AcceptVerbs("GET")]
        public List<UserModel> FindAll()
        {
            return userList;
        }


    }

}




