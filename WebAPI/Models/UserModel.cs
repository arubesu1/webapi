﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models
{
    public class UserModel
    {

        private int id;
        private string name;
        private string login;

        public UserModel()
        {

        }

        public UserModel(int id, string name, string login)
        {
            this.Id = id;
            this.Name = name;
            this.Login = login;
        }

        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;

            }
        }

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public string Login
        {
            get
            {
                return login;
            }
            set
            {
                login = value;

            }

        }





    }
}